@extends('layouts.master.main')

@section('page-title')
    Blogs
@stop

@section('page-content')
    <div class="container">
        <h2>Blogs</h2>
        <hr>

        <table class="table table-striped">

            <thead>
                <tr>
                    <th>S.No.</th>
                    <th>Blog Title</th>
                    <th>Blog Description</th>
                    <th>Added Date</th>
                    <th>Modified Date</th>
                    <th>Action</th>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <td>1</td>
                    <td>sd</td>
                    <td>df</td>
                    <td>sdf</td>
                    <td>sdf</td>
                    <td>sdf</td>
                <tr>

                <tr>
                    <td>2</td>
                    <td>sd</td>
                    <td>df</td>
                    <td>sdf</td>
                    <td>sdf</td>
                    <td>sdf</td>
                <tr>

                <tr>
                    <td>3</td>
                    <td>sd</td>
                    <td>df</td>
                    <td>sdf</td>
                    <td>sdf</td>
                    <td>sdf</td>
                <tr>
            </tbody>
        </table>
    </div>
@stop